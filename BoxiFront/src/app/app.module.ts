import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

//routes
import { AppRoutingModule } from './app-routing.module';
import { APP_ROUTING } from './app.routes';

//Services
import { ProductService } from './services/product.service';
import { SendEmailService } from './services/send-email.service';

//components
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { MattressesComponent } from './components/mattresses/mattresses.component';
import { CombosComponent } from './components/combos/combos.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    HomeComponent,
    MattressesComponent,
    CombosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    APP_ROUTING,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    ProductService,
    SendEmailService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
