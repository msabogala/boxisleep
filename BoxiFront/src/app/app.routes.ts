import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { MattressesComponent } from './components/mattresses/mattresses.component';
import { CombosComponent } from './components/combos/combos.component';


const APP_ROUTES: Routes =
[ 
    { path: 'home', component: HomeComponent },
    { path: 'mattresses', component: MattressesComponent },
    { path: 'combos', component: CombosComponent },
    {path: '**', pathMatch: 'full', redirectTo: 'home'},
    {path: '', pathMatch: 'full', redirectTo: 'home'}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, {useHash: true});
