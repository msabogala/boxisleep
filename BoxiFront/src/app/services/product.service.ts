import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http' 

@Injectable()

export class ProductService{

    private url = 'https://boxisleep-8bd8b.firebaseio.com';
   
    constructor( private http:HttpClient)
    {
        
        console.log('Servicio listo')
    }

    getProducts(){
        return this.http.get(`${this.url}/Productos.json`)
    }
    getCategory(){
        return this.http.get(`${this.url}/Categoria.json`)
    }
    getMedida(){
        return this.http.get(`${this.url}/Medida.json`)
    }
    getTamano(){
        return this.http.get(`${this.url}/Tamano.json`)
    }
    getFirmeza(){
        return this.http.get(`${this.url}/Firmeza.json`)
    }

}