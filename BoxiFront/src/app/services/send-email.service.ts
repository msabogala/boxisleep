import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SendEmailService {
  private url = 'http://localhost:3030/';

  constructor(private http:HttpClient) { }
  send_email(user_name:string, user_email:string, product_name:string):Observable<any>{

    return this.http.post(`${this.url}send-email`,{to: user_email, name: user_name, product:product_name});
    
  }
}
