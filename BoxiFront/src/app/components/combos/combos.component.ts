import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product.service';
import Swal from 'sweetalert2';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import {SendEmailService} from '../../services/send-email.service';

@Component({
  selector: 'app-combos',
  templateUrl: './combos.component.html',
  styleUrls: ['./combos.component.css']
})
export class CombosComponent implements OnInit {

  combo_01_basePrice: number;
  combo_01_listPrice: number;
  combo_01_name: string;
  combo_01_tam: string;
  combo_01_measure: number;
  combo_01_firm: string;

  combo_02_basePrice: number;
  combo_02_listPrice: number;
  combo_02_name: string;
  combo_02_tam: string;
  combo_02_measure: number;
  combo_02_firm: string;

  buttonClicked: number;
  form: FormGroup;

  constructor(private product_service : ProductService, private send_email: SendEmailService) {
    this.form = new FormGroup({
      user_name: new FormControl('', [Validators.required, Validators.minLength(2),Validators.maxLength(21)]),
      user_email: new FormControl('', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]),
    });
   }

  get invalidname(){
    return this.form.get('user_name').invalid && this.form.get('user_name').touched
  }
  get invalidemail(){
    return this.form.get('user_email').invalid && this.form.get('user_email').touched
  }

  ngOnInit() {
    this.product_service.getProducts().subscribe(resp =>{
      console.log(resp)
      this.combo_01_basePrice = resp[0].basePrice;
      this.combo_01_listPrice = resp[0].listPrice;
      this.combo_01_name = resp[0].name;

      this.combo_02_basePrice = resp[1].basePrice;
      this.combo_02_listPrice = resp[1].listPrice;
      this.combo_02_name = resp[1].name;
    })
    this.product_service.getMedida().subscribe(resp =>{
      console.log(resp)
      this.combo_01_measure = resp[0].measure_1;
      this.combo_02_measure = resp[1].measure_2;
    })
    this.product_service.getTamano().subscribe(resp =>{
      console.log(resp)
      this.combo_01_tam = resp[3].simple;
      this.combo_02_tam = resp[2].semidouble;
    })
    this.product_service.getFirmeza().subscribe(resp =>{
      console.log(resp)
      this.combo_01_firm = resp[0].firm;
      this.combo_02_firm = resp[1].soft;
    })
  }
  send_buy_combo01(){
    console.log('Entró')
    console.log(this.form.get('user_name').value.length)
    console.log(this.form.get('user_email').value)
    if(this.form.get('user_email').value.length == 0 && this.form.get('user_email').value.length == 0){
      Swal.fire({
        title: 'Ingrese su nombre y correo',
        text: 'Campos vacíos',
        icon: 'error'
      });
    }
    else{
      this.send_email.send_email(this.form.get('user_name').value, 'correonsanchez@olchonesrem.com', this.combo_01_name).subscribe(resp =>{
        console.log('Swal01')
        Swal.fire({
          title: 'Gracias por elegirnos',
          text: 'La compra fue exitosa',
          icon: 'success'
        });
      },error=>{ 
        console.log(error)    
        Swal.fire({
        title: 'Error enviando correo',
        icon: 'error'
      });})
    }
  }
  send_buy_combo02(){
    console.log('Entró')
    console.log(this.form.get('user_name').value.length)
    console.log(this.form.get('user_email').value)
    if(this.form.get('user_email').value.length == 0 && this.form.get('user_email').value.length == 0){
      Swal.fire({
        title: 'Ingrese su nombre y correo',
        text: 'Campos vacíos',
        icon: 'error'
      });
    }
    else{
      this.send_email.send_email(this.form.get('user_name').value, 'correonsanchez@olchonesrem.com', this.combo_02_name).subscribe(resp =>{
        console.log('Swal01')
        Swal.fire({
          title: 'Gracias por elegirnos',
          text: 'La compra fue exitosa',
          icon: 'success'
        });
      },error=>{ 
        console.log(error)    
        Swal.fire({
        title: 'Error enviando correo',
        icon: 'error'
      });})
    }
  }

}
