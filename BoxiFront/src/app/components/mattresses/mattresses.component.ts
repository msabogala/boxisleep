import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product.service';
import Swal from 'sweetalert2';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import {SendEmailService} from '../../services/send-email.service';

@Component({
  selector: 'app-mattresses',
  templateUrl: './mattresses.component.html',
  styleUrls: ['./mattresses.component.css']
})
export class MattressesComponent implements OnInit {

  colchon_01_basePrice: number;
  colchon_01_listPrice: number;
  colchon_01_name: string;
  colchon_01_tam: string;
  colchon_01_measure: number;
  colchon_01_firm: string;

  colchon_02_basePrice: number;
  colchon_02_listPrice: number;
  colchon_02_name: string;
  colchon_02_tam: string;
  colchon_02_measure: number;
  colchon_02_firm: string;

  buttonClicked: number;
  form: FormGroup;

  constructor( private product_service : ProductService, private send_email: SendEmailService) { 
    this.form = new FormGroup({
      user_name: new FormControl('', [Validators.required, Validators.minLength(2),Validators.maxLength(21)]),
      user_email: new FormControl('', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]),
    }); 
  }
 
  get invalidname(){
    return this.form.get('user_name').invalid && this.form.get('user_name').touched
  }
  get invalidemail(){
    return this.form.get('user_email').invalid && this.form.get('user_email').touched
  }
  ngOnInit() {
    this.product_service.getProducts().subscribe(resp =>{
      console.log(resp)
      this.colchon_01_basePrice = resp[2].basePrice;
      this.colchon_01_listPrice = resp[2].listPrice;
      this.colchon_01_name = resp[2].name;

      this.colchon_02_basePrice = resp[3].basePrice;
      this.colchon_02_listPrice = resp[3].listPrice;
      this.colchon_02_name = resp[3].name;
    })
    this.product_service.getMedida().subscribe(resp =>{
      console.log(resp)
      this.colchon_01_measure = resp[3].measure_4;
      this.colchon_02_measure = resp[2].measure_3;
    })
    this.product_service.getTamano().subscribe(resp =>{
      console.log(resp)
      this.colchon_01_tam = resp[1].queen;
      this.colchon_02_tam = resp[0].double;
    })
    this.product_service.getFirmeza().subscribe(resp =>{
      console.log(resp)
      this.colchon_01_firm = resp[0].firm;
      this.colchon_02_firm = resp[0].firm;
    })
  }
  send_buy_colchon01(){
    console.log('Entró')
    console.log(this.form.get('user_name').value.length)
    console.log(this.form.get('user_email').value)
    if(this.form.get('user_email').value.length == 0 && this.form.get('user_email').value.length == 0){
      Swal.fire({
        title: 'Ingrese su nombre y correo',
        text: 'Campos vacíos',
        icon: 'error'
      });
    }
    else{
      this.send_email.send_email(this.form.get('user_name').value, 'correonsanchez@olchonesrem.com', this.colchon_01_name).subscribe(resp =>{
        console.log('Swal01')
        Swal.fire({
          title: 'Gracias por elegirnos',
          text: 'La compra fue exitosa',
          icon: 'success'
        });
      },error=>{ 
        console.log(error)    
        Swal.fire({
        title: 'Error enviando correo',
        icon: 'error'
      });})
    }
  }
  send_buy_colchon02(){
    console.log('Entró')
    console.log(this.form.get('user_name').value.length)
    console.log(this.form.get('user_email').value)
    if(this.form.get('user_email').value.length == 0 && this.form.get('user_email').value.length == 0){
      Swal.fire({
        title: 'Ingrese su nombre y correo',
        text: 'Campos vacíos',
        icon: 'error'
      });
    }
    else{
      this.send_email.send_email(this.form.get('user_name').value, 'correonsanchez@olchonesrem.com', this.colchon_02_name).subscribe(resp =>{
        console.log('Swal01')
        Swal.fire({
          title: 'Gracias por elegirnos',
          text: 'La compra fue exitosa',
          icon: 'success'
        });
      },error=>{ 
        console.log(error)    
        Swal.fire({
        title: 'Error enviando correo',
        icon: 'error'
      });})
    }
  }
}
