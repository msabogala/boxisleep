# RetoBoxiSleep

**Para clonar el repositorio:**[Link](https://gitlab.com/msabogala/boxisleep.git/ "Repositorio RetoBoxiSleep Gitlab")

**Link para instalar extención cors de google chrome:**[Link](https://chrome.google.com/webstore/detail/allow-cors-access-control/lhobafahddgcelffkeicbaginigeejlf?hl=es// "Extensión cors de google chrome")

Este reto consistió en crear un desarrollo web emulando algunos apartados de la página oficial de BoxiSleep a partir de los requerimientos planteados.
Por lo tanto, se desarrolló una app web usando el framework **Angular** en la parte del frontend, se usó **firebase** para crear la base de datos y se implementó un backend en **node** para realizar la función de enviar correos, la cual dispara un correo de notificación al administrador al momento de que se concrete una compra por parte de un usuario.
En el frontend se crearon cinco componentes que permiten dar una buena presentación y funcionalidad a la págia web. Como también, se implementaron dos servicios, uno para obtener la información de los productos desde firebase y otro que realiza una petición post para el envío de correos electrónicos

Ahora bien, en el backend se implementó la funcionalidad de enviar correo usando principalmente nodemailer.
**Es importante aclarar**  que para un correcto funcionamiento de la funcionalidad es necesario **instalar la extención cors** de Google Chrome, usando el **siguiente enlace** puede acceder a esto [Link](https://chrome.google.com/webstore/detail/allow-cors-access-control/lhobafahddgcelffkeicbaginigeejlf?hl=es/ "Extensión cors de google chrome")

También es necesario activar dicha funcion **cors**, lo cual se realiza en la parte superior derecha de una venta de Google Chrome.


**Para correr el frontend:**

Una vez clonado el repositorio del Gitlab se accede a la carpeta /BoxiFront y desde un termial se ejecuta  **npm install**  y seguidamente  **ng build**  para así instalar las librerías requeridas en el la aplicación web y compilar la misma.

Para ejecutar el frontend, es necesario ejecutar la siguiente línea de comandos  **ng serve -o**
 
**Para correr el backend:**

Una vez clonado el repositorio del Gitlab se accede a la carpeta /BoxiBack y desde un terminal se ejecuta el siguiente comando para instalar las dependencias usadas en la aplicación web: **npm i**

Para correr el Backend se debe ejecutar desde la carpera /BoxiBack **npm start**.

