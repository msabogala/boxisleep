function Email(to, subject, msg) {
    this.to = to;
    this.from = process.env.SENDER_EMAIL;
    this.subject = subject;
    this.text = msg;
}


module.exports =  Email;