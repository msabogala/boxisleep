const Email = require('../models/email');
const nodemailer = require('nodemailer');


module.exports = {
    execute( req, res) {
        try {
            const transporter = createTransporter();
            const msg = 'Hola adminstrador, ' + req.body.name + ' ha comprado el siguiente producto: ' + req.body.product
            const email = new Email(req.body.to, 'Nueva compra en BoxiSleep',msg);
            sendEmail(email, transporter);
            res.status(200).json({res:'Mensaje enviado'});
        } catch (err){
            console.log(err.message);
            res.status(500).json({res: err.message});
        }
    }
}

function createTransporter() {
    return nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: process.env.SENDER_EMAIL,
            pass: process.env.SENDER_EMAIL_PSW
        }
    });
}

function sendEmail(email, transporter) {
    if((email instanceof Email) && (transporter)) {
        transporter.sendMail(email, function(error, info){
        if (error) throw new Error(error.message);
        });
    }
}