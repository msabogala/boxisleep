const express = require('express');
const app = express();
const appRouter = require('./router.js');
const bodyParser = require('body-parser').json();
require('dotenv').config();
const cors= require("cors");
app.get('/', function(req,res) {
    res.send('The server is running');
});

app.use(bodyParser);
app.use(cors());


app.use('/send-email', appRouter);

app.listen(process.env.PORT, function () {
    console.log('Server running...')
})