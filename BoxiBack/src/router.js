const router = require('express').Router();
const emailController = require('./controllers/emailController.js');

router.route('/').post(emailController.execute);

module.exports = router;

